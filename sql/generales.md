# Notas generales de SQL

## UNION

Para unir verticalmente resultados de queries. 

- Es necesario que las los queries a unirse tengan la **misma cantidad de columnas**.
- UNION por defecto borra duplicados

```
(
	SELECT street_address, city, state, postal_code
	FROM customers WHERE street_address IS NOT NULL
)
UNION
(
	SELECT street_address, city, state, postal_code
	FROM dealerships WHERE street_address IS NOT NULL
)
ORDER BY 1;
```

Output:
![unions](resources/images/unions.jpg)

## UNION ALL

Similar a UNION pero no borra duplicados

## CASE WHEN

Permite mepear valores en una columna a otros. Ejemplo

```sql
select 
 (case when p.n <= 3 then p.name else null END) as professor,
 (case when d.n <= 3 then d.name else null END) as doctor,
 (case when s.n <= 3 then s.name else null END) as singer,
 (case when a.n <= 3 then a.name else null END) as actor
FROM
  	(select row_number() over(order by occupation) as n,  name from occupations o where occupation = 'Professor') as p,
	(select row_number() over( order by occupation) as n, name from occupations o where occupation = 'Doctor') as d,
  	(select row_number() over(order by occupation) as n,  name from occupations o where occupation = 'Singer') as s,
	(select row_number() over( order by occupation) as n, name from occupations o where occupation = 'Actor') as a
```

## COALESCE

Remplaza los valores `NULL` por un valor por defecto.

```sql
SELECT first_name, last_name, COALESCE(phone, 'NO PHONE') as phone
  FROM customers ORDER BY 1;
```

## NULLIF

El opuesto de `COALESCE`.

```sql
SELECT customer_id, NULLIF(title, 'Honorable') as title FROM customers c
```

## LEAST/GREATEST

Ayuda para reemplazar valores mayores que o menores que a otro dado.

```sql
SELECT product_id, model, product_type, LEAST(600.00, base_msrp) as base_msrp,
FROM products
WHERE product_type='scooter' 
ORDER BY 1;
```

## CASTING

Sirve para cambiar el tipo de dato de una columna a otra. El formato de uso `columna::datatype`

```sql
SELECT product_id, model, year::TEXT FROM products
```

## DISTINCT

Devuelve solo los valores únicos de una o la cominación de varias columnas.

```sql
SELECT DISTINCT year FROM products ORDER BY 1;
```
Varias columnas
```sql
SELECT DISTINCT year, product_type
FROM products ORDER BY 1, 2;
```

Por ejemplo para saber el número único de estados de una tabla `customers`

```sql
SELECT COUNT(DISTINCT state) FROM customers;
```

## DISTINCT ON

Permite asegurar que solo se retornará una fila cuando una o más columnas son únicas en query. 

```sql
SELECT DISTINCT ON (distinct_column) column_1, column2, ... column_n
  FROM table ORDER BY oreder_column
```

`distinct_column` es la columna que se quiere que sea distinta en el *query*, `column_1`  a `column_n` son columnas que se quieren en el query. Sino se especifica un ORDER BY `order_column`, la primera fila se decidirá aleatoriamente. Ejemplo de uso.

```sql
SELECT DISTINCT ON (first_name)
*
FROM salespeople
ORDER BY first_name, hire_date;
```

## Funciones agregadas

Toman muchas filas y permiten convertirlas en un número.

![agg.jpg](resources/images/major_aggregates.jpg)
## GROUP BY

Se usa para agrupar por columnas, a los grupos se les aplica una función de agregación.


```
           ---grupo1----> aggregate 1
          /
 ------- /
|Dataset|-----grupo2----> aggregate 2
 -------
        \
         \
		  ----grupoN----> aggregate N

```

Ejemplo
```sql
SELECT state, COUNT(*), FROM customers GROUP BY state;

SELECT state, gender, COUNT(*) FROM customers GROUP BY state, gender ORDER BY state, gender;

SELECT product_type, MIN(base_msrp), MAX(base_msrp), AVG(base_msrp), STDDEV(base_msrp)
  FROM products GROUP BY 1
  ORDER BY 1;
```

![gr1](resources/images/group_by_r1.jpg)


## GROUPING SETS

Ayuda a crear agrupaciones por multiples categorías. Genera varios grupos y se encarga de agrupar esos grupos según necesidad.

```sql
SELECT state, gender, COUNT(*)
  FROM customers
  GROUP BY GROUPING SETS (
    (state),
    (gender),
    (state, gender)
  )
  ORDER BY 1, 2;
```
Que da el mismo resultado que.

```sql
(
  SELECT state, NULL as gender, COUNT(*)
  FROM customers  GROUP BY 1, 2
  ORDER BY 1, 2
)
UNION ALL
(
  (
    SELECT state, gender, COUNT(*)
    FROM customers
    GROUP BY 1, 2
    ORDER BY 1, 2
  )
) ORDER BY 1, 2
```

![gs1](resources/images/grouping_sets1.jpg)

## Ordered set aggragate functions

Existen funciones de agregación que dependen del orden del los datos, para usarlas se usa `WITHIN GROUP` como:

![msaggfn1](resources/images/major_set_aggregate_funcs.jpg)


Por ejemplo:

```sql
SELECT PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY base_msrp) AS median
  FROM products;
```
Que retorna:
```
| median double precision |
|-------------------------|
|                  749.99 |
```

## HAVING

Para filtrar usando funciones agregadas.

```sql
SELECT state, COUNT(*) FROM customers
  GROUP BY state
  HAVING COUNT(*)>=1,000
  ORDER BY state
```

## WINDOW FUNCTIONS

Ayudan a crear aplicar funciones agregadas a particiones del mismo *query*. Las particiones se definen con `OVER`. Por ejemplo

```sql
SELECT customer_id, title, first_name, last_name, gender,
  COUNT(*) OVER () as total_customers
  FROM customers
  ORDER BY customer_id;
```
`OVER ()` Es una partición que comprende todo el *query* el resultado es:

![window_1](resources/images/window_1.jpg)

El siguiente ejemplo crea una partición por género, aplica COUNT a cada partición y el resultado lo pone en `total_customers`.
```sql
SELECT customer_id, title, first_name, last_name, gender,
  COUNT(*) OVER (PARTITION BY gender) as total_customers
  FROM customers
  ORDER BY customer_id;
```
![window_2](resources/images/window_2.jpg)

Cuando se usa `ORDER BY` dentro de `OVER` se crea una ventana (window) por cada fila resultado de aplicar ORDER BY a cada grupo o partición. Si se especifica `PARTITION BY` (que seria como `GROUP BY`) y `ORDER BY` dentro de `OVER`, por cada partición se crean múltiples ventanas igual al número de filas resultado de aplicar un ORDER BY a cada partición. Por ejemplo:
```sql
SELECT customer_id, title, first_name, last_name, gender,
  COUNT(*) OVER (PARTITION BY gender ORDER BY customer_id) as total_customers
  FROM customers
  ORDER BY customer_id;
```
Que resulta en:
![window_3](resources/images/window_3.jpg)
Sería, con `PARTITION BY` se divide por género, luego cada partición tiene sus propias ventanas aplicandole `ORDER BY`. A todo eso se aplica `COUNT`

## WINDOW

La palabra reservada `WINDOW` sirve para simplificar la aplicación de window functions. Por ejemplo:
```sql
SELECT customer_id, title, first_name, last_name, gender,
  COUNT(*) OVER w as total_customers,
  SUM(CASE WHEN title IS NOT NULL THEN 1 ELSE 0 END)
      OVER w as total_customers_title
  FROM customers
  WINDOW w AS (PARTITION BY gender ORDER BY customer_id)
  ORDER BY customer_id;
```
Resulta en
![window_4](resources/images/window_4.jpg)

Da el mismo resultado que:
```sql
SELECT customer_id, title, first_name, last_name, gender,
  COUNT(*) OVER (PARTITION BY gender ORDER BY customer_id) as total_customers,
  SUM(CASE WHEN title IS NOT NULL THEN 1 ELSE 0 END)
      OVER (PARTITION BY gender ORDER BY customer_id) as total_customers_title
  FROM customers
  ORDER BY customer_id;
```

### Estadísticas 

Todas las funciones de agregación se pueden usar en window functions, también hay funciones estadísticas útiles.

![window_5](resources/images/window_5.jpg)

