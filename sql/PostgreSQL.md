# POSTGRESQL

## Tamaño de las tablas

```sql
SELECT
  schema_name,
  relname,
  pg_size_pretty(table_size) AS size,
  table_size

FROM (
       SELECT
         pg_catalog.pg_namespace.nspname           AS schema_name,
         relname,
         pg_relation_size(pg_catalog.pg_class.oid) AS table_size

       FROM pg_catalog.pg_class
         JOIN pg_catalog.pg_namespace ON relnamespace = pg_catalog.pg_namespace.oid
     ) t
WHERE schema_name NOT LIKE 'pg_%'
ORDER BY table_size DESC;
```

## Transacciones

Por defecto los *queries* hacen automáticamente **COMMIT** es decir se vuelven permanentes. Mediante las transacciones se puede hacer *rollback* a *queries* anteriores.

- Para iniciar una transacción se utiliza `BEGIN`. Esto es como poner una marca para que se pueda volver al estado anterior de cada *query*.
- Mediante `ROLLBACK` se puede llevar la BD a un estado anterior al **último query ejecutado**.
- Para finalizar el conteo de transacciones y volver al modo **AUTO COMMIT** se ejecuta `END`; (confirmar)
