# NOTita! :poodle:

Colección de notitas útiles :)

Todo el contenido esta bajo liencia GPLv3 para el software y creative commons Attribution 4.0 International.

[![Licence](https://img.shields.io/github/license/Ileriayo/markdown-badges?style=for-the-badge)](./LICENSE)
