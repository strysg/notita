## Convertir mp4 a webm para publicar en la web

Primero se necesita: `ffmpeg libav-tools`.

```
ffmpeg -i ARCHIVO_ENTRADA.mp4 -c:v libvpx-vp9 -b:v 1000k -c:a libvorbis -b:a 96k -threads 0 ARCHIVO_SALIDA.webm
```

## Convertir de m4 a webm y poner otro archivo de audio al video

```
ffmpeg -i VIDEO.mp4 -i AUDIO.ogg -b:v 1000k -b:a 96k -acodec copy -map 0:0 -map 1:0 -threads 0 SALIDA.webm
```

**NOTA:** Ahí `-acodec copy` copia el codec de audio del archivo `AUDIO.ogg` y lo pone en el contenedor webm, dependiendo del archivo de salida podría ser necesario usar otro codec que soporte ese contenedor.

## Redimensionar mp4

Redimensiona a 1100x900 pixeles.

```
ffmpeg -i "2020-10-09.mp4" -c:a 1100:900 -c:a copy Nuevo-2020-10-09.mp4
```
