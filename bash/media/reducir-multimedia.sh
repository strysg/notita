#!/bin/bash

# Reduce el tamaño de imágenes jpg usando convert.
# También redimensiona los videos mp4 a 1100x900

DIRECTORIO=$PWD
PERCENT=33

DESTINO="reducidos"
if [ -e "$DESTINO" ]
then
    echo
else
    mkdir $DESTINO
fi

uso () {
    echo "Uso"
    echo "reducir.sh directorio [percent]"
    echo ""
    echo "Opciones:"
    echo "  directorio:  Directorio las imágenes con extensión .jpg"
    echo "  percent:     Porcentaje de tamaño resultante"
    echo "Ejemplo:"
    echo "  bash reducir.sh $PWD 50"
    echo "  bash reducir.sh /tmp/imagenes 20"
}

reducir () {
    entrada=${1}
    salida=$DESTINO/thumb_${1}
    echo "Reduciendo: '$entrada' a $PERCENT%"
    convert $entrada -resize $PERCENT% $salida
    echo "'$salida'"
    echo
}

reducirmp4 () {
    entrada=${1}
    salida=$DESTINO/resized_${1}
    echo "-- Reduciendo mp4: '$entrada'"
    ffmpeg -i "$entrada" -c:a 1100:900 -c:a copy "$salida"
    echo "'$salida'"
    echo
}

#ffmpeg -i "2020-10-09.mp4" -c:a 1100:900 -c:a copy Nuevo-2020-10-09.mp4
if [ -n "$1" ]
then
    DIRECTORIO=$1
    if [ -n "$2" ]
    then
	PERCENT=$2
    fi
fi

echo "Reducir imágenes:"
echo $DIRECTORIO
ls $DIRECTORIO | grep -e "jpe*g$"
echo "----"

cd $DIRECTORIO
pwd
for archivo in $(ls . | grep -e "jpe*g$")
do
    reducir $archivo $WIDTH $HEIGHT
done


echo "------------------- VIDEOS -----------------"
ls $DIRECTORIO | grep -e "mp4$"
for archivo in $(ls . | grep -e "mp4$")
do
    reducirmp4 $archivo
done

exit 0
