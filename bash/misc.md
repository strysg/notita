# Snippets y notas generales de bash

## Estado de sistema

### Ver uso de memoria RAM
```bash
ps aux | awk '{print $6/1024 " MB\t\t"$11}' | sort -n  | less
```

### Envío de archivos

#### Mandar archivos sin usar servidor web con netcat (nc)

En el directorio fuente, en la **fuente** del los archivos (en este caso la carpeta librecmc/ )
```bash
tar cf - librecmc/ | pv | nc -q 0 159.203.141.16 2223
```
Donde 159.203.141.16 es la ip del destino

Sin comprimir (*sin tar*)
```bash
pv < wikipedia_es_all\(1\).zip | nc -q 0 172.24.0.47 3333
```
172.24.0.47  es la ip destino. 3333 es el puerto.

En la computadora **destino**, en el directorio donde se quiere recibir los archivos
```bash
 nc -lp 2223 |  pv  | tar -C ./ -xvf -
```
#### Backups con gpg a partir de comprimidos

Por ejemplo la carpeta /tmp/
```bash
zip -r - /tmp/ | gpg --symmetric > salida.gpg
```
Comprimirá /tmp y se lo pasa a gpg para que lo cifre y guarde como `salida.gpg'

Otro ejemplo que acelera el proceso en una carpeta con archivos ya comprimidos donde zip
en general no podria comprimirlo mas:
```bash
zip -r - planes/ -x \*.mkv \*.tar.bz2 \*.ipk \*.webm \*.zip \*.tar.gz \*. \*.png \*.jpg \*.jpeg \*.tar.xz | gpg --symmetric > /mnt/droplet/planes.gpg
```

#### sincronizar archivos remotamente con rsync (PULL)

En este caso en un servidor tenia una carpeta enorme y tenia que copiar sus archivos a un servidor local (en mi LAN).

```bash
rsync -azP strysg@rmgss.net:/tmp/wikipedia-offline ./wiki_castellano
```
Lo anterior sincroniza (hace que el contenido de la carpeta fuente y destino sean iguales) usando compresion (-z) y reportando (-P). Previamente he copiado la clave ssh de la maquina local al servidor remoto.

Se llama PULL por que estoy 'jalando' u obteniendo los datos del servidor remoto hacia el local, si en sentido contrario se estaria haciendo cambios en el servidor remoto (esto es peligroso si por ejemplo la carpeta local estaria vacia).

## Snippets

### Iterar sobre un archivo linea por linea con bash
```bash
while read p; do
  echo $p
done <peptides.txt
```

### Obtener la ruta absoluta del script actual

```
# ruta absoluta del script actual (incluído el nombre)
SCRIPT_PATH=$(readlink -f "${BASH_SOURCE}")

# ruta absoluta del direcotrio que contiene el script (sin el nombre del script)
echo "$(dirname -- $SCRIPT_PATH)"
cd $(dirname -- $SCRIPT_PATH)

pwd
```
