# Conexiones 

## Limitar ancho de banda 

Lo siguiente limita el ancho de banda para una interfaz en específico

```
sudo tc qdisc add dev wlp0s20f3 root tbf rate 2548Kbit latency 50ms burst 1540

# wlp0s20f3: Nombre de la interfaz
# rate 2548Kbit: Limita a 2548Kbit
# latency: Agrega latencia de 50 ms
# burst: Una medida por defecto
```

TODO: Agregar comando para restablecer comportamiento normal.

## Crear tunel ssh para jupyter notebook

Esquema:
```
                       ssh |
(Server casa):8890  <----------> (Server remoto):8890
```
Comando creando [remote forwarding](https://www.ssh.com/ssh/tunneling/example):
```
ssh -l strysg -Rn 8890:localhost:8890 rmgss.net
```
También:

```
ssh -l strysg -nNT -R 8890:localhost:8890 rmgss.net
```
