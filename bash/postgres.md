## postgres permission denied to user
```sql
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA <schema_name> TO <username>;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA <schema_name> TO <username>;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA <schema_name> TO <username>;
```

[Ref](https://stackoverflow.com/questions/10352695/grant-all-on-a-specific-schema-in-the-db-to-a-group-role-in-postgresql)

Cambiar dueño

```sql
ALTER TABLE <schema>.<table> OWNER TO <username> ;
```

## postgres crear rol y permitir conexiones

Una vez se crea el rol hay que habilitar el login:

```
ALTER USER user LOGIN;
```

## postgres permitir conexiones externas

Primero en el archivo `/etc/postgresql/12/main/postgresql.conf` cambiar:
```
listen_addresses = '*'
```

Luego en `/etc/postgresql/12/main/pg_hba.conf` agregar:
```
host    all             all              0.0.0.0/0                       md5
host    all             all              ::/0                            md5
```

En el equipo remoto se puede probar con:

```
netcat <ip> 5432
```
